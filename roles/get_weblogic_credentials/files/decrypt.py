import sys
from weblogic.security.internal import *
from weblogic.security.internal.encryption import *

encryptionService = SerializedSystemIni.getEncryptionService(".")
clearOrEncryptService = ClearOrEncryptedService(encryptionService)

encrypted_value = sys.argv[1]

print "decrypted_value=" + clearOrEncryptService.decrypt(encrypted_value)